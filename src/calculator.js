let notExport = 'abc';

function square(x){
    return x*x;
}

const MY_CONST = 123;

export default {square};
